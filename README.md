My config for Linux pc
======================

Installation
------------

You need to install this first:

- https://github.com/gpakosz/.tmux

- https://github.com/robbyrussell/oh-my-zsh

- https://github.com/alexanderjeurissen/ranger_devicons

Then, clone the repo and run the following from your terminal:

```
$ ln -sf dotfiles/.vimrc
$ ln -sf dotfiles/.zshrc
$ ln -sf dotfiles/.tmux.conf.local
$ ln -sf dotfiles/.xsessionrc
$ ln -sf dotfiles/compton.conf

$ mkdir .scripts
$ ln -sf dotfiles/.scripts/*

$ cd ~/.config/ranger
$ ln -sf ~/dotfiles/rc.conf
$ cd ~/.config/i3
$ ln -sf ~/dotfiles/i3/config
$ ln -sf ~/dotfiles/i3/lock.sh
$ cd ~/.config/polybar
$ ln -sf ~/dotfiles/polybar/config
$ ln -sf ~/dotfiles/polybar/launch.sh
$ ln -sf ~/dotfiles/polybar/polypomo

# cd /etc/X11/
# ln -sf ~/dotfiles/X11/xorg.conf
# mkdir -p xorg.conf.d
# cd xorg.conf.d
# ln -sf ~/dotfiles/X11/xorg.conf.d/90-touchpad.conf
```
