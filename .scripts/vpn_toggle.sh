#!/bin/bash

if pidof openvpn; then
    sudo killall openvpn
else
    sudo openvpn  --config ~/Documents/slave.ovpn
fi
