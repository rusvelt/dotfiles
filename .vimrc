call plug#begin('~/.vim/plugged')

Plug 'frazrepo/vim-rainbow'
Plug 'vimwiki/vimwiki'
Plug 'mattn/calendar-vim'
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdcommenter'
Plug 'junegunn/goyo.vim'

"Git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

"Files
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'ctrlpvim/ctrlp.vim'

"Web
"Plug 'mattn/emmet-vim'
"Plug 'alvan/vim-closetag'

"CSS
Plug 'ap/vim-css-color'


"Python
Plug 'vim-python/python-syntax'

"UI
Plug 'ryanoasis/vim-devicons'

cal plug#end()


"UI - - - - - - - - - - - - - -
set number relativenumber
syntax on
set background=dark
set encoding=UTF-8

"for transparent background
hi Normal guibg=NONE ctermbg=NONE

set cursorline
set showmatch           " highlight matching [{()}]
set splitbelow splitright " open split in right

"set colorcolumn=80     " Show vertical line

set incsearch hlsearch ignorecase

set noswapfile
"set nobackup
"set nowb
set autoread

" Use system clipboard
set clipboard+=unnamedplus

set mouse=a

"PLUGINS - - - - - - - - - - - - - -
let NERDTreeQuitOnOpen = 1
let NERDTreeAutoDeleteBuffer = 1


"Some shit - - - - - - - - - -

"folding - - -
set foldmethod=indent
set foldnestmax=1
set nofoldenable
set foldlevel=99


set updatetime=100
set lazyredraw         " redraw only when we need to

set autoindent smartindent
set expandtab          " use spaces on tab
set tabstop=4          " tab length
set softtabstop=4
set shiftwidth=4


"Delete trailing whitespace on save
    autocmd BufWritePre * %s/\s\+$//e


"- - - - - - - - - - - - - - -
"- - - - - MAPPING - - - - - -
"- - - - - - - - - - - - - - -
map <C-n> :NERDTreeToggle<CR>
map <C-h> :GitGutterLineHighlightsToggle<CR>
map <Leader> <Plug>(easymotion-prefix)
map <Leader>g :Goyo<CR>

"turn on run file on save
map <F4> :autocmd BufWritePost * !./runfile.sh <afile>

let g:user_emmet_leader_key=','

let g:rainbow_active = 1


" Python
let g:python_highlight_all = 1


" C - - - - -
" c filetype for .h files
augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h set filetype=c
augroup END

let @c="i#include <stdio.h>\<CR>\<CR>\<CR>int main() {\<CR>\<Esc>i\<Tab>"


let g:vimwiki_list = [{'path': '~/notes/',
                     \  'syntax': 'markdown', 'ext': '.wiki'},
                     \ {'path': '~/vimwiki/',
                     \  'syntax': 'markdown', 'ext': '.wiki'},
                     \ {'path': '~/Documents/life', 'auto_diary_index': 1}]
